import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class TemperatureConverter extends HttpServlet {

    public int tempConverter(String celsius){
        return (Integer.parseInt(celsius) * 9/5 + 32);
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius to Fahrenheit Converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                Integer.toString(tempConverter(request.getParameter("celsius"))) +
                "</BODY></HTML>");
    }


}
